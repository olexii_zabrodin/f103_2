################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/adc.c \
../Src/crc.c \
../Src/gpio.c \
../Src/i2c.c \
../Src/rtc.c \
../Src/stm32f1xx_hal_msp.c \
../Src/stm32f1xx_it.c \
../Src/sys.c \
../Src/syscalls.c \
../Src/sysmem.c \
../Src/system_stm32f1xx.c \
../Src/tim.c \
../Src/tiny_printf.c \
../Src/usb.c 

CPP_SRCS += \
../Src/main.cpp 

OBJS += \
./Src/adc.o \
./Src/crc.o \
./Src/gpio.o \
./Src/i2c.o \
./Src/main.o \
./Src/rtc.o \
./Src/stm32f1xx_hal_msp.o \
./Src/stm32f1xx_it.o \
./Src/sys.o \
./Src/syscalls.o \
./Src/sysmem.o \
./Src/system_stm32f1xx.o \
./Src/tim.o \
./Src/tiny_printf.o \
./Src/usb.o 

C_DEPS += \
./Src/adc.d \
./Src/crc.d \
./Src/gpio.d \
./Src/i2c.d \
./Src/rtc.d \
./Src/stm32f1xx_hal_msp.d \
./Src/stm32f1xx_it.d \
./Src/sys.d \
./Src/syscalls.d \
./Src/sysmem.d \
./Src/system_stm32f1xx.d \
./Src/tim.d \
./Src/tiny_printf.d \
./Src/usb.d 

CPP_DEPS += \
./Src/main.d 


# Each subdirectory must supply rules for building sources it contributes
Src/adc.o: ../Src/adc.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Src/adc.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Src/crc.o: ../Src/crc.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Src/crc.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Src/gpio.o: ../Src/gpio.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Src/gpio.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Src/i2c.o: ../Src/i2c.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Src/i2c.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Src/main.o: ../Src/main.cpp
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"Src/main.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Src/rtc.o: ../Src/rtc.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Src/rtc.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Src/stm32f1xx_hal_msp.o: ../Src/stm32f1xx_hal_msp.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Src/stm32f1xx_hal_msp.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Src/stm32f1xx_it.o: ../Src/stm32f1xx_it.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Src/stm32f1xx_it.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Src/sys.o: ../Src/sys.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Src/sys.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Src/syscalls.o: ../Src/syscalls.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Src/syscalls.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Src/sysmem.o: ../Src/sysmem.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Src/sysmem.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Src/system_stm32f1xx.o: ../Src/system_stm32f1xx.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Src/system_stm32f1xx.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Src/tim.o: ../Src/tim.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Src/tim.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Src/tiny_printf.o: ../Src/tiny_printf.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Src/tiny_printf.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Src/usb.o: ../Src/usb.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Src/usb.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

