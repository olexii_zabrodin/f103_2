################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Custom/Model/MenuModel.cpp \
../Custom/Model/PwmModel.cpp \
../Custom/Model/PwmModelMenuModelItem.cpp \
../Custom/Model/Settings.cpp 

OBJS += \
./Custom/Model/MenuModel.o \
./Custom/Model/PwmModel.o \
./Custom/Model/PwmModelMenuModelItem.o \
./Custom/Model/Settings.o 

CPP_DEPS += \
./Custom/Model/MenuModel.d \
./Custom/Model/PwmModel.d \
./Custom/Model/PwmModelMenuModelItem.d \
./Custom/Model/Settings.d 


# Each subdirectory must supply rules for building sources it contributes
Custom/Model/MenuModel.o: ../Custom/Model/MenuModel.cpp
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"Custom/Model/MenuModel.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Custom/Model/PwmModel.o: ../Custom/Model/PwmModel.cpp
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"Custom/Model/PwmModel.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Custom/Model/PwmModelMenuModelItem.o: ../Custom/Model/PwmModelMenuModelItem.cpp
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"Custom/Model/PwmModelMenuModelItem.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Custom/Model/Settings.o: ../Custom/Model/Settings.cpp
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"Custom/Model/Settings.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

