################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Custom/Controller/MainController.cpp 

OBJS += \
./Custom/Controller/MainController.o 

CPP_DEPS += \
./Custom/Controller/MainController.d 


# Each subdirectory must supply rules for building sources it contributes
Custom/Controller/MainController.o: ../Custom/Controller/MainController.cpp
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"Custom/Controller/MainController.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

