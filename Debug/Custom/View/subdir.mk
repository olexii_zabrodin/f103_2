################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Custom/View/ButtonsPanel.cpp \
../Custom/View/MenuView.cpp 

OBJS += \
./Custom/View/ButtonsPanel.o \
./Custom/View/MenuView.o 

CPP_DEPS += \
./Custom/View/ButtonsPanel.d \
./Custom/View/MenuView.d 


# Each subdirectory must supply rules for building sources it contributes
Custom/View/ButtonsPanel.o: ../Custom/View/ButtonsPanel.cpp
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"Custom/View/ButtonsPanel.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Custom/View/MenuView.o: ../Custom/View/MenuView.cpp
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"Custom/View/MenuView.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

