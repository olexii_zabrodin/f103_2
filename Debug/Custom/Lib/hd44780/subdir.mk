################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Custom/Lib/hd44780/hd44780.c 

OBJS += \
./Custom/Lib/hd44780/hd44780.o 

C_DEPS += \
./Custom/Lib/hd44780/hd44780.d 


# Each subdirectory must supply rules for building sources it contributes
Custom/Lib/hd44780/hd44780.o: ../Custom/Lib/hd44780/hd44780.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Custom/Lib/hd44780/hd44780.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

