################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Custom/Util/SWOTrace.c 

CPP_SRCS += \
../Custom/Util/Primitive.cpp \
../Custom/Util/StorageManager.cpp 

OBJS += \
./Custom/Util/Primitive.o \
./Custom/Util/SWOTrace.o \
./Custom/Util/StorageManager.o 

C_DEPS += \
./Custom/Util/SWOTrace.d 

CPP_DEPS += \
./Custom/Util/Primitive.d \
./Custom/Util/StorageManager.d 


# Each subdirectory must supply rules for building sources it contributes
Custom/Util/Primitive.o: ../Custom/Util/Primitive.cpp
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"Custom/Util/Primitive.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Custom/Util/SWOTrace.o: ../Custom/Util/SWOTrace.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m3 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Custom/Util/SWOTrace.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
Custom/Util/StorageManager.o: ../Custom/Util/StorageManager.cpp
	arm-none-eabi-g++ "$<" -mcpu=cortex-m3 -std=gnu++14 -g3 -DUSE_HAL_DRIVER -DSTM32F103xB -DDEBUG -c -I../Custom/Controller -I../Inc -I../Custom/Model -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Custom/View -I../Drivers/CMSIS/Include -I../Custom/Util -I../Custom/Lib/hd44780 -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/STM32F1xx_HAL_Driver/Inc -O0 -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"Custom/Util/StorageManager.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

