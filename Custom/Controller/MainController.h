/*
 * MainController.h
 *
 *  Created on: Oct 22, 2019
 *      Author: lex
 */

#ifndef CONTROLLER_MAINCONTROLLER_H_
#define CONTROLLER_MAINCONTROLLER_H_

#include "ButtonsPanel.h"
#include "MenuView.h"

class MainController : public ButtonPanelDelegate
{
private:
	ButtonsPanel _buttonsPanel;

	MenuView* _menuView = 0;
	MenuModel* _menuModel = 0;

public:
	virtual void didChangeState(uint8_t state);
	virtual void didChangeStateButtonStop(bool pressed);
	virtual void didChangeStateButtonStart(bool pressed);
	virtual void didChangeStateButtonMenuUp(bool pressed);
	virtual void didChangeStateButtonMenuDown(bool pressed);
	virtual void didChangeStateButtonEnter(bool pressed);
	virtual void didChangeStateButtonCancel(bool pressed);

	void update();
	MainController();
};



#endif /* CONTROLLER_MAINCONTROLLER_H_ */
