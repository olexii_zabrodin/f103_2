#include <PwmModelMenuModelItem.h>
#include "MainController.h"
#include <stdio.h>
#include "tim.h"

static const char* kCpuFreq = "CPU Freq.";
static const char* kCpu = "CPU";
static const char* kCpuModelValue = "STM32F103C8";
static char kCpuValue[6] = {0};
static const char* kScreenLightningMenuModelItemTitle = "LCD Light";
static const char* kLoadPowerMenuModelItemTitle = "Load Power";

MainController::MainController()
{
	 _buttonsPanel.setDelegate(this);

	 PwmModel* loadPowerModel = new PwmModel(&htim4, TIM_CHANNEL_4, TIM_OCPOLARITY_HIGH);
	 PwmModelMenuModelItem* loadPowerMenuModelItem = new PwmModelMenuModelItem(loadPowerModel);
	 loadPowerMenuModelItem->setTitle(kLoadPowerMenuModelItemTitle);

	 MenuModelItem* cpuFrequencyMenuModelItem = new MenuModelItem();
	 cpuFrequencyMenuModelItem->setTitle(kCpuFreq);
	 uint systemFreq = HAL_RCC_GetHCLKFreq() / 1000000;
	 sprintf(kCpuValue, "%uMHz", systemFreq);
	 cpuFrequencyMenuModelItem->setValue(kCpuValue);

	 MenuModelItem* cpuModelTypeMenuModelItem = new MenuModelItem();
	 cpuModelTypeMenuModelItem->setTitle(kCpu);
	 cpuModelTypeMenuModelItem->setValue(kCpuModelValue);

	 PwmModel* screenLightningModel = new PwmModel(&htim3, TIM_CHANNEL_3, TIM_OCPOLARITY_HIGH);
	 PwmModelMenuModelItem* screenLightningMenuModelItem = new PwmModelMenuModelItem(screenLightningModel);
	 screenLightningMenuModelItem->setTitle(kScreenLightningMenuModelItemTitle);

	 const uint8_t menuModelItemsSize = 4;
	 MenuModelItem** menuModelItems = new MenuModelItem*[menuModelItemsSize];
	 menuModelItems[0] = loadPowerMenuModelItem;
	 menuModelItems[1] = cpuFrequencyMenuModelItem;
	 menuModelItems[2] = cpuModelTypeMenuModelItem;
	 menuModelItems[3] = screenLightningMenuModelItem;

	 Rect<unsigned int> menuModelFrame(0, 1, 16, 1);
	 _menuModel = new MenuModel(menuModelItems, menuModelItemsSize);
	 _menuView = new MenuView(menuModelFrame, _menuModel);
}

void MainController::didChangeState(uint8_t state)
{
	printf("A button is pressed");
}

void MainController::didChangeStateButtonStop(bool pressed)
{
	if(pressed)
	{
		 HAL_StatusTypeDef state = HAL_TIM_OC_Stop(&htim2, TIM_CHANNEL_4);
		 if(HAL_OK == state)
		 {

		 }

		 state = HAL_TIM_PWM_Stop(&htim4, TIM_CHANNEL_4);
		 if(HAL_OK == state)
		 {
//			 HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);
		 }
	}
}

void MainController::didChangeStateButtonStart(bool pressed)
{
	if(pressed)
	{
		HAL_StatusTypeDef state = HAL_TIM_OC_Start(&htim2, TIM_CHANNEL_4);
		if(HAL_OK == state)
		{

		}

		__HAL_RCC_TIM4_CLK_ENABLE();
		state = HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_4);
		if(HAL_OK == state)
		{

		}
	}
}

void MainController::didChangeStateButtonEnter(bool pressed)
{
	if(pressed)
	{
		if(_menuModel->editModeEnabled())
		{
			_menuModel->commitEditing();
		}
		else
		{
			_menuModel->startEditing();
		}

		_menuView->setEditModeEnabled(_menuModel->editModeEnabled());
	}
}

void MainController::didChangeStateButtonCancel(bool pressed)
{
	if(pressed)
	{
		if(_menuModel->editModeEnabled())
		{
			_menuModel->cancelEditing();
			_menuView->update();
			_menuView->setEditModeEnabled(_menuModel->editModeEnabled());
		}
	}
}

void MainController::didChangeStateButtonMenuUp(bool pressed)
{
	if(pressed)
	{
		_menuModel->goUp();
		_menuView->update();
	}
}

void MainController::didChangeStateButtonMenuDown(bool pressed)
{
	if(pressed)
	{
		_menuModel->goDown();
		_menuView->update();
	}
}

void MainController::update()
{
	  _buttonsPanel.update();
}
