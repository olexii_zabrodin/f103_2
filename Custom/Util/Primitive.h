/*
 * Primitive.h
 *
 *  Created on: 9 лист. 2019 р.
 *      Author: lex
 */

#ifndef UTIL_PRIMITIVE_H_
#define UTIL_PRIMITIVE_H_

template <typename T>
struct Point
{
	T x = 0;
	T y = 0;

	Point(T initX = 0, T initY = 0)
	{
		x = initX;
		y = initY;
	};
};

template <typename T>
struct Size
{
	T width = 0;
	T height = 0;

	Size(T initWidth = 0, T initHeight = 0)
	{
		width = initWidth;
		height=initHeight;
	};
};

template <typename T>
struct Rect
{
public:
	Point<T> origin;
	Size<T> size;

	Rect(Point<T> initOrigin, Size<T> initSize)
	{
		origin = initOrigin;
		size = initSize;
	};

	Rect(T initX = 0, T initY = 0, T initWidth = 0, T initHeight = 0)
	{
		origin = Point<T>(initX, initY);
		size = Size<T>(initWidth, initHeight);
	};

	Rect(const Rect& initRect)
	{
		origin = initRect.origin;
		size = initRect.size;
	};
};

#endif /* UTIL_PRIMITIVE_H_ */
