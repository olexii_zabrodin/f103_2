/*
 * StorageManager.h
 *
 *  Created on: 24 жовт. 2019 р.
 *      Author: lex
 */

#ifndef UTIL_STORAGEMANAGER_H_
#define UTIL_STORAGEMANAGER_H_

#include "stm32f1xx.h"

class StorageManager {
public:
	StorageManager();
	virtual ~StorageManager();

	uint8_t readLcdLightValue();
	bool writeLcdLightValue(uint8_t);
};

#endif /* UTIL_STORAGEMANAGER_H_ */
