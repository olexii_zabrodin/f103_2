/*
 * MenuView.h
 *
 *  Created on: 8 лист. 2019 р.
 *      Author: lex
 */

#ifndef VIEW_MENUVIEW_H_
#define VIEW_MENUVIEW_H_

#include "MenuModel.h"
#include <Primitive.h>

class MenuView
{
private:
	Rect<unsigned int> _frame;
	MenuModel* _menuModel;

public:
	MenuView(const Rect<unsigned int>& frame, MenuModel* const menuModel);
	void update(void);

	void setEditModeEnabled(bool enabled);
};

#endif /* VIEW_MENUVIEW_H_ */
