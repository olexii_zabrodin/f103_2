/*
 * ButtonsPanel.cpp
 *
 *  Created on: Oct 22, 2019
 *      Author: lex
 */

#include "ButtonsPanel.h"
#include <stdio.h>

void ButtonPanelDelegate::didChangeState(uint8_t state)
{
	printf("Button panel pressed a button %d", state);
}

void ButtonPanelDelegate::didChangeStateButtonStop(bool pressed)
{
	printf("Button panel pressed a button STOP %d", pressed);
}

void ButtonPanelDelegate::didChangeStateButtonStart(bool pressed)
{
	printf("Button panel pressed a button START %d", pressed);
}

void ButtonPanelDelegate::didChangeStateButtonMenuUp(bool pressed)
{
	printf("Button panel pressed a button MENU UP %d", pressed);
}

void ButtonPanelDelegate::didChangeStateButtonMenuDown(bool pressed)
{
	printf("Button panel pressed a button MENU DOWN %d", pressed);
}

void ButtonPanelDelegate::didChangeStateButtonEnter(bool pressed)
{
	printf("Button panel pressed a button ENTER %d", pressed);
}

void ButtonPanelDelegate::didChangeStateButtonCancel(bool pressed)
{
	printf("Button panel pressed a button CANCEL %d", pressed);
}


void ButtonsPanel::update(void)
{
	uint8_t buttons = 0;
	uint8_t buttonPressed = (((GPIO_PIN_RESET == HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_9))) << ButtonPanelStop);
	if ((_state & (1 << ButtonPanelStop)) ^ buttonPressed)
	{
		if(_delegate)
		{
			_delegate->didChangeStateButtonStop(buttonPressed);
		}
	}

	buttons |= buttonPressed;

	buttonPressed = (((GPIO_PIN_RESET == HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15))) << ButtonPanelStart);
	if ((_state & (1 << ButtonPanelStart)) ^ buttonPressed)
	{
		if(_delegate)
		{
			_delegate->didChangeStateButtonStart(buttonPressed);
		}
	}

	buttons |= buttonPressed;

	buttonPressed = (((GPIO_PIN_RESET == HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13))) << ButtonPanelMenuUp);
	if ((_state & (1 << ButtonPanelMenuUp)) ^ buttonPressed)
	{
		if(_delegate)
		{
			_delegate->didChangeStateButtonMenuUp(buttonPressed);
		}
	}

	buttons |= buttonPressed;

	buttonPressed = (((GPIO_PIN_RESET == HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0))) << ButtonPanelStopDown);
	if ((_state & (1 << ButtonPanelStopDown)) ^ buttonPressed)
	{
		if(_delegate)
		{
			_delegate->didChangeStateButtonMenuDown(buttonPressed);
		}
	}

	buttons |= buttonPressed;

	buttonPressed = (((GPIO_PIN_RESET == HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14))) << ButtonPanelEnter);
	if ((_state & (1 << ButtonPanelEnter)) ^ buttonPressed)
	{
		if(_delegate)
		{
			_delegate->didChangeStateButtonEnter(buttonPressed);
		}
	}

	buttons |= buttonPressed;

	buttonPressed = (((GPIO_PIN_RESET == HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_8))) << ButtonPanelCancel);
	if ((_state & (1 << ButtonPanelCancel)) ^ buttonPressed)
	{
		if(_delegate)
		{
			_delegate->didChangeStateButtonCancel(buttonPressed);
		}
	}

	buttons |= buttonPressed;

	if(buttons != _state)
	{
		_state = buttons;

		if(_delegate)
		{
			_delegate->didChangeState(_state);
		}
	}
}

uint8_t ButtonsPanel::state()
{
	return _state;
}


ButtonPanelDelegate* ButtonsPanel::delegate()
{
	return _delegate;
}

void ButtonsPanel::setDelegate(ButtonPanelDelegate* delegate)
{
	_delegate = delegate;
}

//bool dd
//{
//	  if(stored_a0 != a0)
//	  {
//		  if(GPIO_PIN_RESET == a0)
//		  	  {
//		  		  HAL_TIM_StateTypeDef state = HAL_TIM_OC_Start(&htim2, TIM_CHANNEL_4);
//		  		  if(HAL_OK == state)
//		  		  {
//
//		  		  }
//		  	  }
//		  	  else
//		  	  {
//		  		  HAL_TIM_StateTypeDef state = HAL_TIM_OC_Stop(&htim2, TIM_CHANNEL_4);
//		  		  if(HAL_OK == state)
//		  		  {
//
//		  		  }
//		  	  }
//
//		  stored_a0 = a0;
//	  }
//
//}
