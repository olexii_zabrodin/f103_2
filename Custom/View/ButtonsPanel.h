/*
 * ButtonsPanel.h
 *
 *  Created on: Oct 22, 2019
 *      Author: lex
 */

#ifndef VIEW_BUTTONSPANEL_H_
#define VIEW_BUTTONSPANEL_H_

#include "main.h"

typedef enum buttonPanelState
{
	ButtonPanelStop 	= 1,
	ButtonPanelStart,
	ButtonPanelMenuUp,
	ButtonPanelStopDown,
	ButtonPanelEnter,
	ButtonPanelCancel
} ButtonPanelState;

class ButtonPanelDelegate
{
	public:
		virtual void didChangeState(uint8_t state);
		virtual void didChangeStateButtonStop(bool pressed);
		virtual void didChangeStateButtonStart(bool pressed);
		virtual void didChangeStateButtonMenuUp(bool pressed);
		virtual void didChangeStateButtonMenuDown(bool pressed);
		virtual void didChangeStateButtonEnter(bool pressed);
		virtual void didChangeStateButtonCancel(bool pressed);
};

class ButtonsPanel
{
	private:
	  ButtonPanelDelegate* _delegate = 0;
	  uint8_t _state;

	public:
	  void update(void);
	  uint8_t state();

	  ButtonPanelDelegate* delegate();
	  void setDelegate(ButtonPanelDelegate* delegate);
};

#endif /* VIEW_BUTTONSPANEL_H_ */
