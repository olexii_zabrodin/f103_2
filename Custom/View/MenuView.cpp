/*
 * MenuView.cpp
 *
 *  Created on: 8 лист. 2019 р.
 *      Author: lex
 */

#include <MenuView.h>
#include "hd44780.h"
#include <string.h>

MenuView::MenuView(const Rect<unsigned int>& frame, MenuModel* const menuModel)
{
	_frame = frame;
	_menuModel = menuModel;
}

void MenuView::update(void)
{
	lcdGoto(LCD_1st_LINE + _frame.origin.y, _frame.origin.x);

	MenuModelItem* currentMenuItem = _menuModel->currentMenuItem();

	const unsigned int maxLineSize = 17;
	char menuText[maxLineSize] = {0};
	const char* titleStr = currentMenuItem->title();
	strcpy(menuText, titleStr);
	strcat(menuText, ":");
	const char* valueStr = currentMenuItem->value();
	size_t	valueStrSize = strlen(valueStr);
	size_t	titleStrSize = strlen(titleStr);
	size_t spaceSize = maxLineSize - 1 - titleStrSize - 1 - valueStrSize;
	while(0 < spaceSize)
	{
		strcat(menuText, " ");
		--spaceSize;
	}
	strcat(menuText, valueStr);

	lcdPuts(menuText);
}

void MenuView::setEditModeEnabled(bool enabled)
{
	if(_menuModel->editModeEnabled())
	{
		lcdSetMode(VIEW_MODE_DispOn_BlkOn_CrsOff);
		lcdReturn();
	}
	else
	{
		lcdSetMode(VIEW_MODE_DispOn_BlkOff_CrsOff);
		lcdReturn();
	}
}
