/*
 * .cpp
 *
 *  Created on: 24 жовт. 2019 р.
 *      Author: lex
 */

#include "Settings.h"

#include "StorageManager.h"

static Settings* sharedInstance = 0;

Settings* Settings::shared()
{
	if(!sharedInstance)
	{
		sharedInstance =  new Settings();
	}

	return sharedInstance;
}

Settings::Settings()
{

}

Settings::~Settings()
{

}

uint8_t Settings::lcdLightValue()
{
	return 0;
}

bool Settings::setLcdLightValue(uint8_t)
{
	return false;
}
