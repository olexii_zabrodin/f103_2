/*
 * ScreenLightning.h
 *
 *  Created on: 10 лист. 2019 р.
 *      Author: lex
 */

#ifndef MODEL_PWMMODEL_H_
#define MODEL_PWMMODEL_H_

#include "main.h"

class PwmModel
{
public:
//	PwmModel();
	PwmModel(TIM_HandleTypeDef* timer, uint32_t channel, uint32_t polarity);

	uint16_t duty();
	void setDuty(uint16_t duty);	//percent

	bool on();
	void setOn(bool on);

private:
	bool _on;
	uint16_t _pulse;
	uint16_t _period;
	TIM_HandleTypeDef* _timer;
	uint32_t _channel;
	uint32_t _polarity;
};

#endif /* MODEL_PWMMODEL_H_ */
