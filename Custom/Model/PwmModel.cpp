/*
 * ScreenLightning.cpp
 *
 *  Created on: 10 лист. 2019 р.
 *      Author: lex
 */

#include <PwmModel.h>
#include "tim.h"

static const uint16_t kPeriod = 100;
static const uint16_t kPulse = 50;

//static void _setPwmReriod(TIM_HandleTypeDef* timer, uint32_t channel, uint16_t period)
//{
//	HAL_TIM_PWM_Stop(timer, channel); // stop generation of pwm
//
//	timer->Init.Period = period; // set the period duration
//	HAL_TIM_PWM_Init(timer); // reinititialise with new period value
//
// 	HAL_TIM_PWM_Start(timer, channel); // start pwm generation
//}

static void _setPwmPulse(TIM_HandleTypeDef* timer, uint32_t channel, uint32_t polarity, uint16_t pulse)
{
	HAL_TIM_PWM_Stop(timer, channel);

	TIM_OC_InitTypeDef sConfigOC = {0};
	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = pulse < kPeriod ? pulse : kPeriod - 1;
	sConfigOC.OCPolarity = polarity;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	if (HAL_TIM_PWM_ConfigChannel(timer, &sConfigOC, channel) != HAL_OK)
	{
	   Error_Handler();
	}
}

PwmModel::PwmModel(TIM_HandleTypeDef* timer, uint32_t channel, uint32_t polarity)
{
	_timer = timer;
	_channel = channel;
	_polarity = polarity;
	_period = kPeriod;
	_pulse = kPulse;
	_on = true;

	_setPwmPulse(timer, channel, _polarity,_pulse);

	if(_on)
	{
	 	HAL_TIM_PWM_Start(timer, channel);
	}
}

uint16_t PwmModel::duty()
{
	return (kPeriod * _pulse  / 100);
}

void PwmModel::setDuty(uint16_t duty)
{
	_pulse = (duty * 100 / kPeriod);

	_setPwmPulse(_timer, _channel, _polarity, _pulse);

	if(_on)
	{
	 	HAL_TIM_PWM_Start(_timer, _channel);
	}
}

bool PwmModel::on()
{
	return _on;
}

void PwmModel::setOn(bool on)
{
	if(on == _on)
	{
		return;
	}

	if(_on)
	{
		HAL_TIM_PWM_Stop(_timer, _channel);
	}

	_on = on;

	if(_on)
	{
		HAL_TIM_PWM_Start(_timer, _channel);
	}
}



