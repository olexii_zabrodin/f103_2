/*
 * MenuModel.cpp
 *
 *  Created on: 8 лист. 2019 р.
 *      Author: lex
 */

#include "MenuModel.h"

const char* MenuModelItem::title()
{
	return _title;
}

void MenuModelItem::setTitle(const char* title)
{
	_title = title;
}

const char* MenuModelItem::value()
{
	return _value;
}

void MenuModelItem::setValue(const char* value)
{
	_value = value;
}

bool MenuModelItem::isActive()
{
	return _isActive;
}

void MenuModelItem::setIsActive(bool isActive)
{
	_isActive = isActive;
}

//////////////////////////////////////////////////////////////////////////////////////////////
MenuModelItem* MenuModel::currentMenuItem()
{
	if(0 == _size)
	{
		return 0;
	}

	return _menuModelItems[_currentMenuItemIndex];
}

bool MenuModel::goNextMenuItem()
{
	if(0 == _size)
	{
		return 0;
	}

	_currentMenuItemIndex++;
	if(_size <= _currentMenuItemIndex)
	{
		_currentMenuItemIndex = 0;
	}

	return 1;
}

bool MenuModel::goPreviouswMenuItem()
{
	if(0 == _size)
	{
		return 0;
	}

	_currentMenuItemIndex = (0 == _currentMenuItemIndex) ? (_size - 1) : (_currentMenuItemIndex - 1);
	return 1;
}

bool MenuModel::goUp()
{
	if(editModeEnabled())
	{
		return valueUp();
	}
	else
	{
		return goPreviouswMenuItem();
	}
}

bool MenuModel::goDown()
{
	if(editModeEnabled())
	{
		return valueDown();
	}
	else
	{
		return goNextMenuItem();
	}
}

MenuModel::MenuModel(MenuModelItem** menuModelItems, uint8_t size)
{
	_menuModelItems = menuModelItems;
	_size = size;
}

bool MenuModel::editModeEnabled()
{
	return currentMenuItem() ? currentMenuItem()->editModeEnabled() : false;
}

bool MenuModel::editModeAllowed()
{
	return currentMenuItem() ? currentMenuItem()->editModeAllowed() : false;
}

bool MenuModel::startEditing()
{
	return currentMenuItem() ? currentMenuItem()->startEditing() : false;
}

bool MenuModel::cancelEditing()
{
	return currentMenuItem() ? currentMenuItem()->cancelEditing() : false;
}

bool MenuModel::commitEditing()
{
	return currentMenuItem() ? currentMenuItem()->commitEditing() : false;
}

bool MenuModel::valueUp()
{
	return currentMenuItem() ? currentMenuItem()->valueUp() : false;
}

bool MenuModel::valueDown()
{
	return currentMenuItem() ? currentMenuItem()->valueDown() : false;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool MenuEditingProtocol::editModeEnabled()
{
	return false;
}

//bool MenuEditingProtocol::setEditModeEnabled(bool enabled)
//{
//	return false;
//}

bool MenuEditingProtocol::editModeAllowed()
{
	return false;
}

//bool MenuEditingProtocol::setEditModeAllowed(bool allowed)
//{
//	_editModeAllowed = allowed;
//	return true;
//}

bool MenuEditingProtocol::startEditing()
{
	return false;
}

bool MenuEditingProtocol::cancelEditing()
{
	return false;
}

bool MenuEditingProtocol::commitEditing()
{
	return false;
}

bool MenuEditingProtocol::valueUp()
{
	return false;
}

bool MenuEditingProtocol::valueDown()
{
	return false;
}

