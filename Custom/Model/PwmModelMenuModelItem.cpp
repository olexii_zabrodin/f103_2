/*
 * ScreenLightningMenuModelItem.cpp
 *
 *  Created on: 10 лист. 2019 р.
 *      Author: lex
 */

#include <PwmModelMenuModelItem.h>


PwmModelMenuModelItem::PwmModelMenuModelItem(PwmModel* pwmModel)
{
	_pwmModel = pwmModel;
	uint16_t duty = _pwmModel->duty();
	sprintf(kValue, "%u%%", duty);
	setValue(kValue);
}

bool PwmModelMenuModelItem::valueUp()
{

	uint16_t duty = _pwmModel->duty();
	duty += 10;
	if(duty <= 100)
	{
		_pwmModel->setDuty(duty);

		sprintf(kValue, "%u%%", duty);

		return true;
	}

	return false;
}

bool PwmModelMenuModelItem::valueDown()
{
	uint16_t duty = _pwmModel->duty();
	duty -= 10;
	if(duty < 100) //unsigned
	{
		_pwmModel->setDuty(duty);

		sprintf(kValue, "%u%%", duty);

		return true;
	}

	return false;
}

bool PwmModelMenuModelItem::editModeEnabled()
{
	return _editModeEnabled;
}

bool PwmModelMenuModelItem::editModeAllowed()
{
	return true;
}

bool PwmModelMenuModelItem::startEditing()
{
	if(_editModeEnabled)
	{
		return false;
	}

	_editedDuty = _pwmModel->duty();
	_editModeEnabled = true;
	return true;
}

bool PwmModelMenuModelItem::cancelEditing()
{
	if(!_editModeEnabled)
	{
		return false;
	}

	if(_pwmModel->duty() != _editedDuty)
	{
		_pwmModel->setDuty(_editedDuty);
		sprintf(kValue, "%u%%", _editedDuty);
	}

	_editModeEnabled = false;
	return true;
}

bool PwmModelMenuModelItem::commitEditing()
{
	if(!_editModeEnabled)
	{
		return false;
	}

	setValue(kValue);
	_editModeEnabled = false;
	return true;
}
