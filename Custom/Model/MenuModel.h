/*
 * MenuModel.h
 *
 *  Created on: 8 лист. 2019 р.
 *      Author: lex
 */

#ifndef MODEL_MENUMODEL_H_
#define MODEL_MENUMODEL_H_

#include "main.h"

class MenuEditingProtocol
{
protected:
//	bool _editModeEnabled = false;
//	bool _editModeAllowed = false;

public:
	virtual bool editModeEnabled();
	virtual bool editModeAllowed();
	virtual bool startEditing();
	virtual bool cancelEditing();
	virtual bool commitEditing();
	virtual bool valueUp();
	virtual bool valueDown();
};

class MenuModelItem:public MenuEditingProtocol
{
protected:
	const char* _title = 0;
	const char* _value = 0;

	bool _isActive = false;

public:
	virtual const char* title();
	virtual void setTitle(const char* title);
	virtual const char* value();
	virtual void setValue(const char* value);

	virtual bool isActive();
	virtual void setIsActive(bool isActive);
};

class MenuModel:public MenuEditingProtocol
{
private:
	MenuModelItem** _menuModelItems = 0;
	uint8_t _currentMenuItemIndex = 0;
	uint8_t _size = 0;

	bool goNextMenuItem();
	bool goPreviouswMenuItem();

public:
	MenuModelItem* currentMenuItem();
	bool goUp();
	bool goDown();

	MenuModel(MenuModelItem** menuModelItems, uint8_t size);

	virtual bool editModeEnabled();
	virtual bool editModeAllowed();
	virtual bool startEditing();
	virtual bool cancelEditing();
	virtual bool commitEditing();
	virtual bool valueUp();
	virtual bool valueDown();
};

#endif /* MODEL_MENUMODEL_H_ */
