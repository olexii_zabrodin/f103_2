/*
 * Settings.h
 *
 *  Created on: 24 жовт. 2019 р.
 *      Author: lex
 */

#ifndef MODEL_SETTINGS_H_
#define MODEL_SETTINGS_H_

#include "stm32f1xx.h"

class Settings {
public:
	static Settings* shared();

	uint8_t lcdLightValue();
	bool setLcdLightValue(uint8_t);

private:
	Settings();
	virtual ~Settings();

	uint8_t _lcdLightValue;
	uint8_t _lcdLightOn;
	uint8_t _buttonSoundOn;
};

#endif /* MODEL_SETTINGS_H_ */
