/*
 * ScreenLightningMenuModelItem.h
 *
 *  Created on: 10 лист. 2019 р.
 *      Author: lex
 */

#ifndef MODEL_PWMMODELMENUMODELITEM_H_
#define MODEL_PWMMODELMENUMODELITEM_H_

#include <PwmModel.h>
#include "MenuModel.h"
#include <stdio.h>

class PwmModelMenuModelItem:public MenuModelItem
{
private:
	bool _editModeEnabled = false;
	PwmModel* _pwmModel;
	uint16_t _editedDuty;
	char kValue[5] = {0};

public:
	virtual bool editModeEnabled();
	virtual bool editModeAllowed();
	virtual bool startEditing();
	virtual bool cancelEditing();
	virtual bool commitEditing();
	virtual bool valueUp();
	virtual bool valueDown();

	PwmModelMenuModelItem(PwmModel* pwmModel);
};

#endif /* MODEL_PWMMODELMENUMODELITEM_H_ */
